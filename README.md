## x1qksx-user 12 SP1A.210812.016 G981NKSU1GVE4 release-keys
- Manufacturer: samsung
- Platform: kona
- Codename: x1q
- Brand: samsung
- Flavor: x1qksx-user
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: G981NKSU1GVE4
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: ko-KR
- Screen Density: undefined
- Fingerprint: samsung/x1qksx/x1q:11/RP1A.200720.012/G981NKSU1GVE4:user/release-keys
- OTA version: 
- Branch: x1qksx-user-12-SP1A.210812.016-G981NKSU1GVE4-release-keys
- Repo: samsung/x1q
