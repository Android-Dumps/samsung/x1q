#
# Copyright (C) 2022 The Android Open Source Project
# Copyright (C) 2022 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_x1q.mk

COMMON_LUNCH_CHOICES := \
    omni_x1q-user \
    omni_x1q-userdebug \
    omni_x1q-eng
